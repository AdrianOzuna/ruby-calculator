require_relative 'functions'

#Ask user for the required numbers
puts "Insert the first number: "
first_number = gets.to_f

puts "Insert the second number "
second_number = gets.to_f

#Ask user what operation he wants to make
puts "Do you want to [add], [substract], [multiply], [divide]"
user_choice = gets.chomp

case user_choice.downcase
when "add"
    puts add(first_number, second_number)
when "substract"
    puts substract(first_number, second_number)
when "multiply"
    puts multiplicate(first_number, second_number)
when "divide"
    puts divide(first_number, second_number)
else
    puts "I couldn't understant. Please try again with a valid operation"
end
